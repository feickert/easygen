This example is to run a very simple LO ttbar production on lxplus

To generate the Sherpa events, execute:
```bash
./submit_sherpa.py
```
The output will end with `Submitted 1234567!`, the job with ID 1234567 should have two subjobs each with a random seed, and each producing an output HepMC file. Each subjob is running the `run.sh` script which runs from a lxbatch htcondor job based on the input container from cvmfs. Each subjob puts the Sherpa run card in place and generates 10k events from a random seed that is different for each subjob.

Now you need to wait for job 1234567 to finish. Once it's finished you can execute the rivet run. This relies on the job ID to have been recorded in the CLUSTERID.txt file.

To run Rivet over your Sherpa HepMC files you can execute the following:
```bash
./submit_rivet.py
```
This run will run the `rivet.sh` script to analyse the HepMC files from the previous step by running them through the `MC_TTBAR` rivet routine to produce a `Sherpa.yoda` output file. Finally, it then plots the results using `rivet-mkhtml`. The output folder should contain the `Sherpa.yoda` file and `rivet-plots/` directory.
